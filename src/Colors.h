#ifndef PLANUM_COLORS_22082022
#define PLANUM_COLORS_22082022
#include <Adafruit_NeoPixel.h>


// Constants storing colors in the format required for NeoPixel
// =================================================
uint32_t BLACKColor = 0;
uint32_t WHITEColor = Adafruit_NeoPixel::Color(255, 255, 255);

uint32_t REDColor = Adafruit_NeoPixel::Color(255, 0, 0);
uint32_t GREENColor = Adafruit_NeoPixel::Color(0, 255, 0);
uint32_t BLUEColor = Adafruit_NeoPixel::Color(0, 0, 255);

uint32_t YELLOWColor =  Adafruit_NeoPixel::Color(255, 255, 0);
uint32_t ORANGEColor =  Adafruit_NeoPixel::Color(255, 100, 0);
uint32_t PURPLEColor = Adafruit_NeoPixel::Color(255, 0, 255);
uint32_t CIANColor =  Adafruit_NeoPixel::Color(0, 255, 255);

uint32_t LIGHTREDColor = Adafruit_NeoPixel::Color(255, 51, 51);
uint32_t LIGHTGREENColor = Adafruit_NeoPixel::Color(102, 204, 0);
uint32_t LIGHTBLUEColor = Adafruit_NeoPixel::Color(0, 128, 255);
uint32_t LIGHTYELLOWColor =  Adafruit_NeoPixel::Color(255, 255, 51);
uint32_t LIGHTORANGEColor =  Adafruit_NeoPixel::Color(255, 150, 0);
uint32_t LIGHTPURPLEColor = Adafruit_NeoPixel::Color(146, 78, 125);
uint32_t LIGHTCIANColor =  Adafruit_NeoPixel::Color(102, 255, 255);
// =================================================


// Setting the backlight colors depending on the condition
// =================================================
uint32_t* HOLDColor = &WHITEColor;
uint32_t* SLEEPColor = &BLUEColor; 
uint32_t* MOVEColor = &YELLOWColor; 
uint32_t* HOMMINGColor = &CIANColor; 

uint32_t* RECIVEColor = &GREENColor;
uint32_t* COMEBACKColor = &ORANGEColor;

uint32_t* SETCORRECTIONColor = &LIGHTORANGEColor;
uint32_t* SAVECORRECTIONColor = &LIGHTGREENColor;
uint32_t* CLEARCORRECTIONColor = &LIGHTREDColor;

uint32_t* ERRORColor = &REDColor;
// =================================================

#endif