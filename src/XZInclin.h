#pragma once
#ifndef XZInclin_11022023
#define XZInclin_11022023

#include <math.h>
#include <Arduino.h>

namespace XZInclin {
    constexpr double ang = 45;                    // axis tilt angle
    constexpr double tilt = 2*(90 - ang) - 90;    // angle of inclination manipulator below horizon

    constexpr double ang_r = radians(ang);
    constexpr double tilt_r = radians(tilt);

    // horisont shift of the lower position
    // Latex formula 
    // horizonShift = acos(1 - \frac{sin(tilt)}{sin(180° - ang - tilt)*cos(ang)})
    constexpr double horizonShift = acos( 1 - sin(tilt_r) / (sin(M_PI - ang_r - tilt_r) * cos(ang_r)) );

    // Latex formula
    // K\tiny1 = \normalsize \frac{sin(ang)}{sin(180°-ang-tilt)}
    constexpr double K1 = sin(ang_r) / sin(M_PI - ang_r - tilt_r);

    // Latex formula 
    // horizonCorr = acos(\frac{1 + K\tiny1\normalsize ^2 - cos^2(ang) * sin^2(horizonShift)}{2*K\tiny1})
    constexpr double horizonAzCorr = acos(
                        (1 + K1*K1 - cos(ang_r)*cos(ang_r) * sin(horizonShift)*sin(horizonShift)) / \
                        (2 * K1)
                        );

    double calcElevation(double az, double el);
    double calcAzimuth(double az, double el);
}
#endif