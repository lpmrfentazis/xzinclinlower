#pragma once
#ifndef PLANUM_POWER_24022023
#define PLANUM_POWER_24022023

#include <Arduino.h>
#include "Pinout.h"
#include "GyverFilters.h"

class PowerControl{
    private:

        int voltagePin = 27;
        int currentPin = 28;

        int correctionV = 43.5;
        int correctionC = 417.3;
        
        float voltageK = 0.0093;
        float currentK = 0.0002744;
        
        GKalman voltageFilter = GKalman(0.15, 0.2, 0.2);
        GKalman currentFilter = GKalman(0.15, 0.2, 0.2);

        float minV = 18.0;
        float maxV = 25.2;

        float current = 0;
        float voltage = 0;
        float percent = 0;

    public:
        PowerControl(int voltagePin, int currentPin) {
            this->voltagePin = voltagePin;
            this->currentPin = currentPin;
        }

        inline float getCurrent() {
            return current;
        }

        inline float getVoltage() {
            return voltage;
        }

        inline float getPercent() {
            return percent;
        }

        inline float getAnalogV() {
            return voltageFilter.filtered(analogRead(voltagePin));
        }
        
        inline float getAnalogC() {
            return currentFilter.filtered(analogRead(currentPin));
        }

        void setMinMaxV(float minV, float maxV) {
            this->minV = minV;
            this->maxV = maxV;
        }

        void setCorrection(float correctionV, float correctionC) {
            this->correctionV = correctionV;
            this->correctionC = correctionC;
        }

        void tick() {
            voltage = voltageK * (voltageFilter.filtered(analogRead(voltagePin)) / 10 * 10 - correctionV);
            current = currentK * (currentFilter.filtered(analogRead(currentPin)) / 10 * 10 - correctionC);

            if (voltage < 0)
                voltage = 0;

            percent = ((voltage - minV) / (maxV - minV)) * 100;
        }

};
#endif