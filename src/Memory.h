#include <Arduino.h>
#include <EEPROM.h>


uint16_t start = 0;

typedef struct {
    /* data */
    byte calibrationSpeed=0;
    byte motorSpeed=0;
    byte azimuthRedution=0;
    byte elevationRedution=0;
    int16_t azimuthSteps=0;
    int16_t elevationSteps=0;
    int16_t azimuthCorrection=0;
    int16_t elevationCorrection=0;
    byte azInv=0;
    byte elInv=0;
    byte hash=0;
} Settings;


bool operator==(Settings& rhs, Settings& lhs) {
    byte* s1 = (byte*) &rhs;
    byte* s2 = (byte*) &lhs;

    int16_t len = sizeof(Settings);

    while(len--)
        if (s1[len] != s2[len])
            return false;

    return true;   
}

/*
  Name  : CRC-8
*/
byte getHash(byte* data, int16_t length) {
    byte hash = 0;
    int16_t i = 0;
    while (length--) {
        hash += *(data + i);
        ++i;
    }
    return hash;
}


class Memory {
    uint16_t size;
    uint16_t start;

    Memory(uint16_t size, uint16_t start) {
        if (size > 4096)
            size = 4096;

        this->size = size;

        if (start > size) 
            start = 0;
            
        this->start = start;

        EEPROM.begin(this->size);
    }

    // check start flags. 0xAA is 10101010
    bool checkMemory() {
        return (EEPROM[start] == 0xAA && EEPROM[start+1] == 0xAA);
    }   

    // if no data return settings.hash = 0
    Settings readSettings() {
        Settings settings;
        
        if (checkMemory()) {
            EEPROM.get(start+2, settings);

            byte hash = getHash((byte*) &settings, sizeof(settings));
            
            if (hash != settings.hash) {
                settings.hash = 0;
            }
        }

        return settings;
    }

    bool writeSettings(Settings settings) {
        EEPROM.write(start, 0xAA);
        EEPROM.write(start+1, 0xAA);

        EEPROM.put(start+2, settings);
        Settings t;

        EEPROM.get(start+2, t);

        EEPROM.commit();

        return settings == t;
    }

    ~Memory() {
        EEPROM.end();
    }
};



