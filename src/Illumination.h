#ifndef PLANUM_ILLUMINATION_22082022
#define PLANUM_ILLUMINATION_22082022

#include <Adafruit_NeoPixel.h>
#include "Colors.h"
#include "Station.h"

#define LENGHTOFTAIL 10
#define HOLDDELAY 10 // in seconds


// A class that implements asynchronous operation of the backlight
//
// To use it, you should call .update() as often as possible.
//
// It also implies the presence of a separate controlled backlight on the back cover,
// but it is not used now, and the signal to the backlight is simply duplicated.
class LEDStrip {
  private:
    Adafruit_NeoPixel strip;
    int count;
    uint8_t brightness;
    STATUS state;

    unsigned long lastTick;
    unsigned long timeout;
    unsigned long lastUpdate;

    int LEDCounter;

  public:
    LEDStrip(int count, int pin, uint8_t brightness) {
        this->strip =  Adafruit_NeoPixel(count, pin, NEO_GRB + NEO_KHZ800);
        this->brightness = brightness;
        this->count = count;
        this->state = SLEEP;
      
        this->lastTick = 0;
        this->timeout = 0;
        this->lastUpdate = 0;

        this->LEDCounter = 0;

    }

    // Initializing and filling the backlight with black
    void init() {
        strip.begin();
        strip.setBrightness(brightness);

        strip.fill(BLACKColor, 0, count);
        strip.show();
    }

    void clearTimeout() {
        timeout = millis();
    }

    // Updates the status of the station, compare it with the value of the global enum of the COMMAND type from Station.h
    void tick() {
        // TIMEOUTS !!!
        //if 
        //if (status == "")
        if ((status == SETCORRECTION || status == MOVE) && (millis() - timeout > 5000) && timeout != 0) 
            status = SLEEP;

        if(state != status) {
            strip.fill(BLACKColor, 0, count);
            LEDCounter=0;
            state = status;

            lastUpdate = millis();
            update();
        }

        if (state == SLEEP && (millis() - lastTick > 100))
            update();

    }

    // The service method. directly updates the backlight
    void update() {
        if (state == HOLD) {
            strip.fill(*HOLDColor, 0, count);
            strip.show();
            delay(HOLDDELAY * 1000);
            status = SLEEP;
        }
        if (state == SLEEP) {
            
            if (LEDCounter > count + LENGHTOFTAIL) {
                LEDCounter = 0;
                strip.fill(BLACKColor, 0, count);
                lastTick = millis();
            }
            
            if (millis() - lastTick > 80) {
                if (LEDCounter - LENGHTOFTAIL >= 0)
                   strip.setPixelColor(LEDCounter-LENGHTOFTAIL, BLACKColor);

                strip.setPixelColor(LEDCounter, *SLEEPColor);
                ++LEDCounter;
                lastTick = millis();
            } 
        
        }
        
        else if (state == MOVE) {
            strip.fill(*MOVEColor, 0, count);
            timeout = millis();
        }

        else if (state == HOMMING) {
            strip.fill(*HOMMINGColor, 0, count);
        }
      
        else if (state == RECIVE) {
            strip.fill(*RECIVEColor, 0, count);
            timeout = millis();
        }

        else if (state == COMEBACK) {
            strip.fill(*COMEBACKColor, 0, count);
        }
        
        else if (state == SETCORRECTION) {
            strip.fill(*SETCORRECTIONColor, 0, count);
            timeout = millis();
        }

        else if (state == SAVECORRECTION) {
            strip.fill(*SAVECORRECTIONColor, 0, count);
        }

        else if (state == CLEARCORRECTION) {
            strip.fill(*CLEARCORRECTIONColor, 0, count);
        }

        else if (state == ERROR) {
            strip.fill(*ERRORColor, 0, count);
        }
      

        strip.show();

    }

    void setBrightness(uint8_t brightness){
        this->brightness = brightness;
    }

    ~LEDStrip() = default;
};

#endif