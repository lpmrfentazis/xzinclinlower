#include <math.h>
#include "XZInclin.h"

double XZInclin::calcElevation(double az, double el) {
    double az_r = radians(az);
    double el_r = radians(el);

    double rot = acos(1 - (cos(el_r)*cos(el_r))/(cos(ang_r)*cos(ang_r) + sin(el_r)*cos(ang_r)*cos(ang_r)));

    // Latex formula
    // elMotor = 180° - acos(1 - \frac{cos^2(el)}{cos^2(ang) + sin(el)*cos^2(ang)))})
    double elMotor = 180 - degrees(rot);

    return elMotor;
}

double XZInclin::calcAzimuth(double az, double el) {
    double az_r = radians(az);
    double el_r = radians(el);

    double rot = acos(1 - (cos(el_r)*cos(el_r))/(cos(ang_r)*cos(ang_r) + sin(el_r)*cos(ang_r)*cos(ang_r)));
    double azCorr = 0;
    
    if (el != 90) {
        azCorr += horizonAzCorr;
        azCorr += acos((1 + cos(el_r)*cos(el_r) - 2*cos(XZInclin::ang_r)*cos(XZInclin::ang_r) + 2*cos(XZInclin::ang_r)*cos(XZInclin::ang_r) * cos(M_PI - XZInclin::horizonShift - rot) + sin(el_r)*sin(el_r))/(2*cos(el_r)));
    }
    
    // Latex formula
    // azCorrection = horizonAzCorr + acos(\frac{1 + cos^2(el) - 2*cos^2(ang) + 2*cos^2(ang)*cos(180° - horizonShift - rot) - sin^2(el)}{2*cos(el)})
    return az - degrees(azCorr);
}